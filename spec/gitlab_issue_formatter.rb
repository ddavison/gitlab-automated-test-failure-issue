# frozen_string_literal: true

# formats failures represented by a GitLab Issue

require 'rspec/core/formatters/base_formatter'
require 'gitlab'

class GitlabIssueFormatter < RSpec::Core::Formatters::BaseFormatter
  RSpec::Core::Formatters.register self, :example_failed

  def initialize(output)
    @output = output
    super

    @gitlab_client = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: ENV['GITLAB_PRIVATE_TOKEN']
    )
  end

  def example_passed(notification)
    # do something with the passed test.
  end

  # On failure, create a GitLab Issue
  def example_failed(notification)
    created_issue = @gitlab_client.create_issue('ddavison/gitlab-automated-test-failure-issue',
      "Failure in #{notification.example.file_path}",
      description: <<~MARKDOWN
        **Failure location**: #{notification.example.location}

        **Description**: #{notification.example.full_description}
        # Stacktrace

        ```
        #{notification.example.exception.message}
        ```

        **Failed at**: #{Time.now.strftime('%Y-%m-%d %H:%M:%S %Z%z')}

        /assign @ddavison
        /due in 1 week
        /label ~"test-failure"
      MARKDOWN
    )

    @output << "Issue: #{created_issue['web_url']}"
  end
end
