# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Failing spec' do
  it 'fails and reports failure as a GitLab issue' do
    expect(false).to eq(true)
  end
end
