# gitlab-automated-test-failure-issue Example

This is an example project to show how one can create GitLab Issues when a test failure occurs.

## How to use

Generate a GitLab personal access token.

```shell
$ bundle install
$ GITLAB_PRIVATE_TOKEN=<private-token> bundle exec rspec
```
